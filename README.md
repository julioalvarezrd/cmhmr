# Compu Mundo Hiper Mega Red

## Descripcion del proyecto.
Compu Mundo Hiper Mega Red, es un blog de noticias tecnologicas, escrito en HTML 5 por un grupo de estudiantes de la universidad dominicana O&M filiar La Romana.

## Participantes

- [ ] [Julio Alvarez](https://gitlab.com/julioalvarezrd) - Documentacion, Pagina sobre nosotros, Enlazar paginas.
- [ ] [Ambrocio de la cruz](https://gitlab.com/Ambrociojr) - Pagina de Inicio.
- [ ] [Hilari Solano](https://gitlab.com/HILARI2001) - Pagina de Contacto.

## Boceto
![Boceto](img/boceto.png)

## Entrega:
Sabado 27 de Agosto 2022
